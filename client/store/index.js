import Vue from 'vue'
import Vuex from 'vuex'

import * as types from './types'

// import { loadNormItemsFake as loadNormItems, loadNormInfoFake as loadNormInfo } from './api'
import { loadNormItems, loadNormInfo } from './api'

import _ from 'lodash'

Vue.use(Vuex)

const state = {
	isError: false,
	errorMessage: '',

	normData: false,
	rootLoading: false,
	treeRoot: [],
}

// additional data in row
// _level: number,
// _uuid: string,
// _expanded: bool,
// _parentUuid: string,
// _children: bool
// _loading: bool

const getters = {
	getTreeRoot (state) {
		return state.treeRoot
	},

	isTreeRootLoading (state) {
		return state.rootLoading
	},

	isError (state) {
		return state.isError
	},
	getError (state) {
		return state.errorMessage
	},

	normData (state) {
		return state.normData
	},

	getRows (state) {
		const rows = state.treeRoot

        // filter rows which should be hidden (parent is collapsed)
		const filterFunc = i => {
			return i._expanded === false
		}

		const hiddenParents = rows.filter(filterFunc).map(i => i._uuid)
		let lastLength = 0
		while (hiddenParents.length !== lastLength) {
			lastLength = hiddenParents.length

			rows.filter(i => hiddenParents.indexOf(i._parentUuid) !== -1).forEach(i => {
				if (hiddenParents.indexOf(i._uuid) === -1) {
					hiddenParents.push(i._uuid)
				}
			})
		}

		return rows.filter(i => hiddenParents.indexOf(i._parentUuid) === -1)
	},

}

function findRowByUuid (state, uuid) {
	return _.find(state.treeRoot, { _uuid: uuid })
}
function findRowIndexByUuid (state, uuid) {
	return _.findIndex(state.treeRoot, { _uuid: uuid })
}

function uuid () {
	function s4 () {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1)
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4()
}

const actions = {
	initialNormCode ({ dispatch, commit }, { code }) {
		commit(types.SET_TREE_ROOT_LOADING, true)

		loadNormInfo(code)
			.then(data => {
				if (! data) {
					return commit(types.SET_ERROR, 'Nelze načíst informace o normě.')
				}

				commit(types.SET_NORM_DATA, data)

				const norm = Object.assign({}, data, {
					_level: 0,
					_uuid: uuid(),
				})

				commit(types.SET_TREE_ROOT, [norm])
				commit(types.SET_TREE_ROOT_LOADING, false)

				return norm
			})
			.then(norm => {
				actions.loadNormItemsByCode({ commit, dispatch }, {
					parent: norm,
					loadRecursive: false,
				})
			})
			.catch(() => {
				return commit(types.SET_ERROR, 'Nelze načíst informace o normě.')
			})
	},

	loadNormItemsByCode ({ commit, dispatch }, { parent, loadRecursive }) {
		const code = parent.code

		commit(types.SET_NODE_LOADING, {
			source: parent,
			loading: true,
		})

		return loadNormItems(code)
			.then(items => {
				const hasItems = !! (items && items.length)

				if (hasItems) {
					commit(types.TREE_APPEND, {
						source: parent,
						payload: items.map(i => {
							i._level = parent._level + 1
							i._parentUuid = parent._uuid
							i._expanded = false
							i._uuid = uuid()

							return i
						}),
					})
				}

				commit(types.UPDATE_ROW, {
					source: parent,
					payload: {
						_expanded: hasItems,
						_children: hasItems,
						_loading: false,
					},
				})

				if (hasItems && loadRecursive) {
					items.forEach(i => {
						dispatch('loadNormItemsByCode', { parent: i, loadRecursive })
					})
				}
			})
			.catch(() => {
				commit(types.UPDATE_ROW, {
					source: parent,
					payload: {
						_children: false,
						_loading: false,
					},
				})
			})
	},

	expandAll ({ commit, state, dispatch }) {
		commit(types.TREE_EXPAND_ALL)

		const expandNodes = state.treeRoot
        .filter(i => i._expanded !== true)
        .filter(i => i._loading !== true)
        .filter(i => typeof i._children === 'undefined' || i._children === true)

		expandNodes.forEach(i => {
			if (typeof i._children === 'undefined') {
				dispatch('loadNormItemsByCode', { parent: i, loadRecursive: true })
			} else {
				commit(types.UPDATE_ROW, {
					source: i,
					payload: {
						_expanded: true,
					},
				})
			}
		})
	},

	collapseAll ({ commit }) {
		commit(types.TREE_COLLAPSE_ALL)
	},

	rowClick ({ state, commit, dispatch }, source) {
		const row = findRowByUuid(state, source._uuid)

		if (! row || row._loading) {
			return
		}

		if (row._children === false) {
			return
		}

		if (row._expanded) {
			return commit(types.UPDATE_ROW, {
				source,
				payload: {
					_expanded: false,
				},
			})
		}

		if (! row._expanded) {
			if (typeof row._children !== 'undefined') {
				return commit(types.UPDATE_ROW, {
					source,
					payload: {
						_expanded: true,
					},
				})
			}

      // else load norm
			return dispatch('loadNormItemsByCode', { parent: source })
		}
	},
}

const mutations = {
	[types.SET_TREE_ROOT] (state, tree) {
		state.treeRoot = tree
	},
	[types.SET_TREE_ROOT_LOADING] (state, loading) {
		state.rootLoading = loading
	},

	[types.UPDATE_ROW] (state, { source, payload }) {
		const itemIndex = findRowIndexByUuid(state, source._uuid)
		state.treeRoot.splice(itemIndex, 1, _.assign({}, state.treeRoot[itemIndex], payload))
	},

	[types.SET_NODE_LOADING] (state, { source, loading }) {
		const itemIndex = findRowIndexByUuid(state, source._uuid)
		state.treeRoot.splice(itemIndex, 1, _.assign({}, state.treeRoot[itemIndex], {
			_loading: loading,
		}))
	},

	[types.TREE_APPEND] (state, { source, payload }) {
		const itemIndex = findRowIndexByUuid(state, source._uuid)
		state.treeRoot.splice(itemIndex + 1, 0, ...payload)
	},

	[types.TREE_EXPAND_ALL] (state, codes) {},
	[types.TREE_EXPAND_CODES] (state, codes) {},

	[types.SET_NORM_DATA] (state, normData) {
		state.normData = normData
	},

	[types.SET_ERROR] (state, errorMessage) {
		state.isError = true
		state.errorMessage = errorMessage
	},

	[types.TREE_COLLAPSE_ALL] (state) {
		for (const index in state.treeRoot) {
			if (! state.treeRoot.hasOwnProperty(index)) {
				continue
			}

			state.treeRoot.splice(index, 1, {
				...state.treeRoot[index],
				_expanded: false,
			})
		}
	},
}

const store = new Vuex.Store({
	state,
	getters,
	mutations,
	actions,
})

export default store
