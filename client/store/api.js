import Vue from 'vue'

/* The completionNorms api is intended to be run on the same server as veit-is hosting the API.*/
const apiBaseUrl = 'http://localhost:3000/api/completionNorms/'

export function loadNormInfo (normCode) {
	return Vue.axios.get(apiBaseUrl + normCode)
		.then(response => response.data)
}

export function loadNormItems (normCode) {
	return Vue.axios.get(apiBaseUrl + normCode + '/items')
		.then(response => response.data)
}

export function loadNormInfoFake (normCode) {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve(JSON.parse(JSON.stringify({
				id: '1A60000101',
				code: 'VB3N0001',
				name: 'BAT3v0.2 terminál',
				quantity: '1',
			})))
		}, 2000)
	})
}

export function loadNormItemsFake (normCode) {
	if (normCode === 'VB3N0001') {
		return new Promise(resolve => {
			setTimeout(() => {
				resolve(JSON.parse(JSON.stringify(vbn)))
			}, 1500)
		})
	}

	if (normCode === 'MELE1361') {
		return new Promise(resolve => {
			setTimeout(() => {
				resolve(JSON.parse(JSON.stringify(mele1361)))
			}, 1500)
		})
	}

	if (normCode === 'MELE1346') {
		return new Promise(resolve => {
			setTimeout(() => {
				resolve(JSON.parse(JSON.stringify(mele1346)))
			}, 2500)
		})
	}

	if (normCode === 'MELE1345') {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				reject(JSON.parse(JSON.stringify([])))
			}, 2500)
		})
	}

	return Promise.reject([])
}

const vbn = [
	{
		position: '1',
		id: 'PGB0000101',
		code: 'MELE1361',
		name: 'Deska plošných spojů BAT3v0.2 terminál zcela osazená',
		quantity: '1',
	},
	{
		position: '3',
		id: 'QGB0000101',
		code: 'MELE1066',
		name: 'Krytka držáku baterie CR2032; BHSD-2032-COVER',
		quantity: '1',
	},
	{
		position: '6',
		id: '1DB0000101',
		code: 'MELE1346',
		name: 'Deska plošných spojů BAT3v0.2 terminál, osazená v Racomu',
		quantity: '1',
	},
	{
		position: '4',
		id: 'TGB0000101',
		code: 'MELE0121',
		name: 'Baterie knoflíková CR2032 lithiová, Vinnic',
		quantity: '1',
	},
	{
		position: '5',
		id: 'UGB0000101',
		code: 'MELE1359',
		name: 'Colibri iMX6 DualLite 512MB',
		quantity: '1',
	},
	{
		position: '6',
		id: 'RGB0000101',
		code: 'MELE1300',
		name: 'Zasouvací svorkovnice na kabel, 5 pólů, šroubovací; CTB922VG/5S',
		quantity: '2',
	},
]

const mele1346 = [
	{
		position: '1',
		id: 'LFB0000101',
		code: 'MELE1345',
		name: 'Deska plošných spojů BAT3v0.2 terminál neosazená',
		quantity: '1',
	},
	{
		position: '2',
		id: 'KFB0000101',
		code: 'MELE1347',
		name: 'Součástky desky plošných spojů BAT3v0.2 terminál, dodávané VEIT',
		quantity: '1',
	},
]

const mele1361 = [{
	position: '1',
	id: '1DB0000101',
	code: 'MELE1346',
	name: 'Deska plošných spojů BAT3v0.2 terminál, osazená v Racomu',
	quantity: '1',
},
{
	position: '5',
	id: 'HGB0000101',
	code: 'MELE1292',
	name: 'Kondenzátor elektrolytický 1mF/35V; ESY108M035AL4AA',
	quantity: '2',
},
{
	position: '8',
	id: 'MGB0000101',
	code: 'MELE1289',
	name: 'ETHernetový konektor RJ45; ARJC02-111009D',
	quantity: '1',
},
{
	position: '9',
	id: 'XGB0000101',
	code: 'MELE1350',
	name: 'Mikrotlačítko, THT; B3F-1000',
	quantity: '1',
},
{
	position: '11',
	id: 'NGB0000101',
	code: 'MELE1302',
	name: 'Header svorkovnice, 5 pólů, THT do DPS; CTB9302/5',
	quantity: '2',
},
{
	position: '13',
	id: 'IGB0000101',
	code: 'MELE0080',
	name: 'Kolíková lišta přímá 1 řada, 2,54mm, 1x40 pinů; S1G40',
	quantity: '0,15',
},
{
	position: '14',
	id: 'KGB0000101',
	code: 'MELE0656',
	name: 'RS485/RS422 Transceiver; DIL8; LT1785CN8',
	quantity: '2',
},
{
	position: '15',
	id: 'WGB0000101',
	code: 'MELE1349',
	name: 'Konektor USB A do DPS, zásuvka 90°; USB1X90',
	quantity: '4',
},
]
