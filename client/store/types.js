
export const SET_TREE_ROOT = 'tree/setRoot'
export const SET_TREE_ROOT_LOADING = 'tree/setRootLoading'

export const TREE_APPEND = 'tree/append'

export const TREE_COLLAPSE_ALL = 'tree/collapseAll'
export const TREE_EXPAND_ALL = 'tree/expandAll'
export const TREE_EXPAND_CODES = 'tree/expandCodes'

export const ROW_CLICK = 'tree/rowClick'
export const UPDATE_ROW = 'tree/UPDATE_ROW'
export const SET_NODE_LOADING = 'tree/setNodeLoading'

export const SET_NORM_DATA = 'tree/setNormData'

export const SET_ERROR = 'tree/setError'
