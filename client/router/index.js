import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '../views/Home'
import NormView from '../views/Norm'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			component: HomeView,
		},
		{
			path: '/norm/:normCode',
			component: NormView,
		},
	],
})
