'use strict'

const path = require('path')
const express = require('express')
const app = express()

app.use(express.static('dist'))

app.use('*', function (req, res, next) {
	res.sendFile(path.join(__dirname, 'dist/index.html'))
})

const port = process.env.PORT || 3000

app.listen(port, function () {
	console.log(`Serving norm-parts-macro at ${port}!`)
})
