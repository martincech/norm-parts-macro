# norm-parts-macro - OBSOLETE!

**Existing stuff has been moved to the https://bitbucket.org/flexiana/veit-is-ui**

## General Info
To start:

```bash
$ npm install
```

To develop:

```bash
npm run dev
```

To build for production:

```bash
npm run build
```

To lint you code:

```bash
$ npm run lint
```

## API

The api for retrieving completion norm and its items is implemented in [veit-is](https://bitbucket.org/flexiana/veit-is/src/HEAD/src/veit/modules/completion_norms/routes.clj).
Ultimately both UI and veit-is should run on the same server.
However, for local development you need to enable CORS somehow, e.g. by using [Chrome extension](https://chrome.google.com/webstore/category/extensions?hl=en-US)


## Deployment 

### Building, deployment & running app
Step 1) run `$ npm install` in app root

Step 2) run `$ npm run build` which will create `dist/` folder

Step 3a) run http server, which will serve `dist/` directory and redirect ALL requests to `index.html` EXCEPT for existing files.

Step 3b) if you have node.js, run `$ npm start` which will start basic express server

Note for step 3b) - set ENV variable PORT to value you need. Default port is 3000.
Tip for step 3b) - install [pm2](https://github.com/Unitech/pm2) (process manager) which will handle app crash, autorestart, etc.


## Usage

Call `host.tld/norm/:normCode`, where `:normCode` stands for the norm code from the user input

Calling `host.tld` will show the information, how to use this macro.


### Fake data
In case of problems when using real API (because of CORS), you can use fake api data (see /client/store/api.js).